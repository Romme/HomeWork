#include <EEPROM.h>

#define RedLed 10
#define GreenLed 3
#define BlueLed 11

#define redP A4
#define greenP A5
#define blueP A3

int n = 0;

int prevB1 = 0, prevB2 = 0, B2_num = 0;

int red = 0, green = 0, blue = 0, sumRGB;

int redPtr, greenPtr, bluePtr, sumPtr;

void setup() {
  Serial.begin(9600);
  for (int a = 0; a < 100; a++) {
    int value = EEPROM.read(a);
    Serial.print(a);
    Serial.print("\t");
    Serial.print(value);
    Serial.println();
    delay(50);
  }

  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(7, INPUT);
  pinMode(8, INPUT);

}

void make_color(int red, int green, int blue) {
  analogWrite(RedLed, red);
  analogWrite(GreenLed, green);
  analogWrite(BlueLed, blue);

}

bool check_color () {
  int i;
  for (i = 1; i < n * 3 + 1; i += 3)
    if (EEPROM.read(i) == red)
      if (EEPROM.read(i + 1) == green)
        if (EEPROM.read(i + 2) == blue) {
          Serial.println("This color already exists.");
          return false;
        }
  return true;
}

void write_color () {
  EEPROM.write(n * 3 + 1, red);
  EEPROM.write(n * 3 + 2, green);
  EEPROM.write(n * 3 + 3, blue);
  n++;
  EEPROM.write(0, n);
}

void read_color () {
  red = EEPROM.read((B2_num * 3) % (n * 3) + 1);
  green = EEPROM.read((B2_num * 3) % (n * 3) + 2);
  blue = EEPROM.read((B2_num * 3) % (n * 3) + 3);
  make_color(red, green, blue);
  B2_num++;
}

void loop() {
  n = EEPROM.read(0);
  int But1 = digitalRead(7);
  int But2 = digitalRead(8);

  redPtr = analogRead(redP);
  greenPtr = analogRead(greenP);
  bluePtr = analogRead(blueP);



  if (  abs(sumPtr  - (redPtr + greenPtr + bluePtr)) >= 20) {
    sumPtr = redPtr + greenPtr + bluePtr;
    red = map(redPtr, 0, 1023, 0, 255);
    green = map(greenPtr, 0, 1023, 0, 255);
    blue = map(bluePtr, 0, 1023, 0, 255);
    make_color(red, green, blue);
    Serial.println("*******************Color by Ptr*******************");
  }


  if (But2 - prevB2 == 1) {
    read_color();
    Serial.print("Color num = ");
    Serial.println(B2_num % n);
  }
  else if (But1 - prevB1 == 1) {
    if (check_color())
      write_color();
  }
  else
    make_color(red, green, blue);



  prevB2 = But2;
  prevB1 = But1;

  delay(50);
}
