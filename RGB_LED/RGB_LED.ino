#define RedLed 6
#define GreenLed 5
#define BlueLed 3
int pause = 100;
int r, g, b;
void setup() {
  Serial.begin(9600);
  Serial.println("The available commands to set the color:");
  Serial.println("red, green, blue, yellow, violet, white,");
  Serial.println("turquoise, pink, blue-ish, random.");
  Serial.println("The available commands to set the brightness: hi, lo.");
  Serial.println("To turn the led off: off.");
}
void make_color(int red, int green, int blue) {
  analogWrite(RedLed, red);
  analogWrite(GreenLed, green);
  analogWrite(BlueLed, blue);
  r = red;
  g = green;
  b = blue;
}
void make_color(String color) {
  if (color.equals("yellow"))
    make_color(255, 50, 0); //yellow
  else if (color == "red")
    make_color(255, 0, 0); //red
  else if (color == "green")
    make_color(0, 255, 0); //green
  else if (color ==  "blue")
    make_color(0, 0, 255); // blue
  else if (color ==  "violet")
    make_color(150, 0, 255); //violet
  else if (color == "turquoise")
    make_color(20, 52, 52); //turquoise
  else if (color == "119104105116101")
    make_color(255, 255, 255); //white
  else if (color ==  "pink")
    make_color(214, 7, 80); //pink
  else if (color ==  "blue-ish")
    make_color(42, 20, 130); //blue-ish
  else if (color == "random") //random
    make_color(random(256), random(256), random(256));    
  else if (color ==  "off")
    make_color(0, 0, 0); //off
  else if (color == "lo") { //lo
    r /= 2;
    g /= 2;
    b /= 2;
    make_color((r), (g), (b));   
  }
  else if (color == "hi") { //hi
    r *= 2;
    g *= 2;
    b *= 2;
    r%=255;
    g%=255;
    b%=255;
    make_color((r), (g), (b));
  }
  delay(pause);
}
void loop() {
  String input;
  input = Serial.readString();
  make_color(input);
 
}
