int DIR1 = 4;
int SPEED1 = 5;
int DIR2 = 7;
int SPEED2 = 6;
int pause = 2;


#define Ptr1 A1
#define Ptr2 A2
#define Ptr3 A3
#define Ptr4 A4

void setup() {
  pinMode(DIR1, OUTPUT);
  pinMode(DIR2, OUTPUT);
  pinMode(SPEED1, OUTPUT);
  pinMode(SPEED2, OUTPUT);
  Serial.begin(9600);
}



void go_forward(int velocity) {
  digitalWrite(DIR1, HIGH);
  digitalWrite(DIR2, HIGH);
  analogWrite(SPEED1, velocity);
  analogWrite(SPEED2, velocity);
}


void go_backward(int velocity) {
  digitalWrite(DIR1, LOW);
  digitalWrite(DIR2, LOW);
  analogWrite(SPEED1, velocity);
  analogWrite(SPEED2, velocity);
}



void right(int velocity) {
  digitalWrite(DIR1, LOW);
  digitalWrite(DIR2, HIGH);
  analogWrite(SPEED1, velocity);
  analogWrite(SPEED2, velocity);
}

void left(int velocity) {
  digitalWrite(DIR1, HIGH);
  digitalWrite(DIR2, LOW);
  analogWrite(SPEED1, velocity);
  analogWrite(SPEED2, velocity);
}

void loop() {
  float vP1 = analogRead(Ptr1);
  float vP2 = analogRead(Ptr2);
  float vP3 = analogRead(Ptr3);
  float vP4 = analogRead(Ptr4) / 2;

  if (vP1 + vP2 > vP3 + vP4) {
    if (vP1 / vP2 >= 1.2) {
      right(255);
      delay(pause);
    }
    else if (vP2 / vP1 >= 1.2) {
      left(255);
      delay(pause);
    }
    else
      go_forward(255);

  }
  else {
    if (vP3 / vP4 >= 1.2) {
      right(255);
      delay(pause);
    }
    else if (vP4 / vP3 >= 1.2) {
      left(255);
      delay(pause);
    }
    else
      go_backward(255);
  }

  Serial.print(vP1);
  Serial.print("\t");
  Serial.print(vP2);
  Serial.print("\t");
  Serial.print(vP3);
  Serial.print("\t");
  Serial.print(vP4);
  Serial.print("\n");


}
