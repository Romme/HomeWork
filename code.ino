#define POT A1 
#define LDR A0 
#define LED 11 
#define LED2 12 
#define LED3 6
#define BUZZER_PIN 8 
int BUT1 = 9;
int FLAG = 1;
void setup() {
  pinMode(LED, OUTPUT);  
  pinMode(LED2, OUTPUT);  
  pinMode(LED3, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  Serial.begin(9600);
  tone(BUZZER_PIN, 500, 50);
  delay(250);
  tone(BUZZER_PIN, 100, 150);
  delay(250);
  tone(BUZZER_PIN, 500, 200);
  delay(250);
  tone(BUZZER_PIN, 200, 300);
}
void myAnalogWrite(int pinNum, int val){
  if(val<0 or val>255){
    return;
  }
  int filoment = int(val*1000./255.);
 
  digitalWrite(pinNum, LOW);
  delayMicroseconds(1000 - filoment);
  return; 
}
void loop() {
  int POTval = analogRead(POT);
  int LDRval = analogRead(LDR);
  int Button = digitalRead(BUT1);
  int z, brightness;
  brightness = POTval/4;
  z = 255-0.249*LDRval;
  Serial.println(LDRval);
  
  if(Button==1){   
    tone(BUZZER_PIN, POTval, 20);   
    digitalWrite(LED2, HIGH);
    delay(50);
  }
  else
    digitalWrite(LED2, LOW);
   
  if(!FLAG and LDRval>800 )
    FLAG++;
    
  if(LDRval<=800){    
    digitalWrite(LED3, HIGH);
   
    if(FLAG){
      tone(BUZZER_PIN, 392, 350);
      delay(350);
      tone(BUZZER_PIN, 392, 350);
      delay(350);
      tone(BUZZER_PIN, 392, 350);
      delay(350);
      tone(BUZZER_PIN, 311, 250);
      delay(250);
      tone(BUZZER_PIN, 466, 100);
      delay(100);
      tone(BUZZER_PIN, 392, 350);
      delay(350);
      tone(BUZZER_PIN, 311, 250);
      delay(250);
      tone(BUZZER_PIN, 466, 100);
      delay(100);
      tone(BUZZER_PIN, 392, 700);
      delay(700);
      
      tone(BUZZER_PIN, 587, 350);
      delay(350);
      tone(BUZZER_PIN, 587, 350);
      delay(350);
      tone(BUZZER_PIN, 587, 350);
      delay(350);
      tone(BUZZER_PIN, 622, 250);
      delay(250);
      tone(BUZZER_PIN, 466, 100);
      delay(100);
      tone(BUZZER_PIN, 369, 350);
      delay(350);
      tone(BUZZER_PIN, 311, 250);
      delay(250);
      tone(BUZZER_PIN, 466, 100);
      delay(100);
      tone(BUZZER_PIN, 392, 700);
      delay(700);
      
      tone(BUZZER_PIN, 784, 350);
      delay(350);
      tone(BUZZER_PIN, 392, 250);
      delay(250);
      tone(BUZZER_PIN, 392, 100);
      delay(100);
      tone(BUZZER_PIN, 784, 350);
      delay(350);
      tone(BUZZER_PIN, 739, 250);
      delay(250);
      tone(BUZZER_PIN, 698, 100);
      delay(100);
      tone(BUZZER_PIN, 659, 100);
      delay(100);
      tone(BUZZER_PIN, 622, 100);
      delay(100);
      tone(BUZZER_PIN, 659, 450);
      delay(450);
      
      tone(BUZZER_PIN, 415, 150);
      delay(150);
      tone(BUZZER_PIN, 554, 350);
      delay(350);
      tone(BUZZER_PIN, 523, 250);
      delay(250);
      tone(BUZZER_PIN, 493, 100);
      delay(100);
      tone(BUZZER_PIN, 466, 100);
      delay(100);
      tone(BUZZER_PIN, 440, 100);
      delay(100);
      tone(BUZZER_PIN, 466, 450);
      delay(450);
      
      tone(BUZZER_PIN, 311, 150);
      delay(150);
      tone(BUZZER_PIN, 369, 350);
      delay(350);
      tone(BUZZER_PIN, 311, 250);
      delay(250);
      tone(BUZZER_PIN, 466, 100);
      delay(100);
      tone(BUZZER_PIN, 392, 750);
      delay(750);
      //delay(5000);
      FLAG--;
    }
  }
  else
    digitalWrite(LED, LOW);
  analogWrite(LED, brightness);
  analogWrite(LED3, z);
    delay(50);
  myAnalogWrite(LED, z);                     
}
