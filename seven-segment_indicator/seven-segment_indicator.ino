int ClockPin = 11;
int LatchPin = 12;
int DataPin = 13;
int pause = 500;
int pause1 = 80;

void setup() {
  pinMode(ClockPin, OUTPUT);
  pinMode(LatchPin, OUTPUT);
  pinMode(DataPin, OUTPUT);
  digitalWrite(LatchPin, HIGH);
  Serial.begin(9600);
  Serial.println("The available commands: ");
  Serial.println("A, B, C, D, E, F, G, H, I, J,");
  Serial.println("L, N, O, P, Q, R, S, T, U, Y,");
  Serial.println("1, 2, 3, 4, 5, 6, 7, 8, 9, 0");
}
void turnOff () {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00000000);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
}
void getZero() {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b11111100);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getOne () {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00110000);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getTwo() {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b11011010);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getThree() {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b01111010);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getFour() {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00110110);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getFive () {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b01101110);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getSix () {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b11101110);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getSeven () {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00111000);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getEight() {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b11111110);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void getNine() {
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b01111110);
  digitalWrite(LatchPin, HIGH);
  delay(pause);
}
void get_AllNumbers () {
  getZero();
  getOne ();
  getTwo();
  getThree();
  getFour();
  getFive();
  getSix();
  getSeven();
  getEight();
  getNine();
}

void turnInfinity() {
  //ledA
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00001000);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
  //ledB
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00010000);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
  //ledG
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00000010);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
  //ledE
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b10000000);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
  //ledD
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b01000000);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
  //ledC
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00100000);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
  //ledG
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00000010);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
  //ledF
  digitalWrite(LatchPin, LOW);
  shiftOut(DataPin, ClockPin, LSBFIRST, 0b00000100);
  digitalWrite(LatchPin, HIGH);
  delay(pause1);
}

void checkCharacter() {
  char symbol = Serial.read();
  switch (symbol) {
    case 'A':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b10111110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'B':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b11100110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'C':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b11001100);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'D':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b11110010);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'E':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b11001110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'F':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b10001110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'G':
      getSix();
      turnOff ();
      break;
    case 'H':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b10110110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'I':
      getOne();
      turnOff ();
      break;
    case 'J':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b01110000);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'L':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b11000100);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'N':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b10100010);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'O':
      getZero();
      turnOff ();
      break;
    case 'P':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b10011110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'Q':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b00111110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'R':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b10000010);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'S':
      getFive();
      break;
    case 'T':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b11000110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'U':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b11110100);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case 'Y':
      digitalWrite(LatchPin, LOW);
      shiftOut(DataPin, ClockPin, LSBFIRST, 0b01110110);
      digitalWrite(LatchPin, HIGH);
      delay(pause);
      turnOff ();
      break;
    case '0':
      getZero();
      turnOff ();
      break;
    case '1':
      getOne();
      turnOff ();
      break;
    case '2':
      getTwo();
      turnOff ();
      break;
    case '3':
      getThree();
      turnOff ();
      break;
    case '4':
      getFour();
      turnOff ();
      break;
    case '5':
      getFive();
      turnOff ();
      break;
    case '6':
      getSix();
      turnOff ();
      break;
    case '7':
      getSeven();
      turnOff ();
      break;
    case '8':
      getEight();
      turnOff ();
      break;
    case '9':
      getNine();
      turnOff ();
      break;
    default:
      turnInfinity();
  }
}

void loop() {
  checkCharacter();
  // get_AllNumbers();
  // delay(pause);
  //turnInfinity();
  // turnInfinity();
}
